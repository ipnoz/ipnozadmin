<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Tests\unit\Service\Tags;

use Codeception\Test\Unit;
use Ipnoz\AdminBundle\Service\Tabs\TabsMenu;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TabsMenuTest extends Unit
{
    public function test_with_empty_tabs_list(): void
    {
        $requestStack = new RequestStack();
        $requestStack->push(new Request(['tab' => 'anyValue']));

        $tabsMenu = new TabsMenu($requestStack);
        $tabsMenu->setupActiveTab();

        $this->assertSame([], $tabsMenu->getTabs());
    }

    public function test_the_requested_tab_name_is_the_priority(): void
    {
        $requestStack = new RequestStack();
        $requestStack->push(new Request(['tab' => 'test-id-3']));

        $tabsMenu = new TabsMenu($requestStack);
        $tabsMenu->addTab('test-tab-1', 'test-id-1', 'test-name-1');
        $tabsMenu->addTab('test-tab-2', 'test-id-2', 'test-name-2', true);
        $tabsMenu->addTab('test-tab-3', 'test-id-3', 'test-name-3');
        $tabsMenu->setupActiveTab();
        $tabs = $tabsMenu->getTabs();

        $this->assertFalse($tabs['test-tab-1']->isActive);
        $this->assertFalse($tabs['test-tab-2']->isActive);
        $this->assertTrue($tabs['test-tab-3']->isActive);
    }

    public function test_with_the_isdefault_parameter_set(): void
    {
        $requestStack = new RequestStack();
        $requestStack->push(new Request());

        $tabsMenu = new TabsMenu($requestStack);
        $tabsMenu->addTab('test-tab-1', 'test-id-1', 'test-name-1');
        $tabsMenu->addTab('test-tab-2', 'test-id-2', 'test-name-2', true);
        $tabsMenu->addTab('test-tab-3', 'test-id-3', 'test-name-3');
        $tabsMenu->setupActiveTab();
        $tabs = $tabsMenu->getTabs();

        $this->assertFalse($tabs['test-tab-1']->isActive);
        $this->assertTrue($tabs['test-tab-2']->isActive);
        $this->assertFalse($tabs['test-tab-3']->isActive);
    }


    public function test_the_first_element_is_the_final_default(): void
    {
        $requestStack = new RequestStack();

        $tabsMenu = new TabsMenu($requestStack);
        $tabsMenu->addTab('test-tab-1', 'test-id-1', 'test-name-1');
        $tabsMenu->addTab('test-tab-2', 'test-id-2', 'test-name-2');
        $tabsMenu->addTab('test-tab-3', 'test-id-3', 'test-name-3');
        $tabsMenu->setupActiveTab();
        $tabs = $tabsMenu->getTabs();

        $this->assertTrue($tabs['test-tab-1']->isActive);
        $this->assertFalse($tabs['test-tab-2']->isActive);
        $this->assertFalse($tabs['test-tab-3']->isActive);
    }
}
