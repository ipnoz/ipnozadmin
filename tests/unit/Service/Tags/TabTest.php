<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Tests\unit\Service\Tags;

use Codeception\Test\Unit;
use Ipnoz\AdminBundle\Service\Tabs\Tab;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TabTest extends Unit
{
    public function test_constructor(): void
    {
        $tab = new Tab($id = 'id-field', $name = 'name-field', $isDefault = true);

        $this->assertSame($id, $tab->id);
        $this->assertSame($name, $tab->name);
        $this->assertSame($isDefault, $tab->isDefault);
    }
}
