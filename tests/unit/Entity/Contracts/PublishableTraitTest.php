<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Tests\unit\Entity\Contracts;

use Codeception\Test\Unit;
use Ipnoz\AdminBundle\Entity\Contracts\OnlineStatus;
use Ipnoz\AdminBundle\Entity\Contracts\PublishableTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PublishableTraitTest extends Unit
{
    public function test_publish_at_getter_and_setter(): void
    {
        /** @var PublishableTrait $publishableTrait */
        $publishableTrait = $this->getMockForTrait(PublishableTrait::class);

        $dateTime  = new \DateTime('-1 day');
        $publishableTrait->setPublishAt($dateTime);

        $this->assertSame($dateTime, $publishableTrait->getPublishAt());
    }

    public function test_publish_until_getter_and_setter(): void
    {
        /** @var PublishableTrait $publishableTrait */
        $publishableTrait = $this->getMockForTrait(PublishableTrait::class);

        $dateTime  = new \DateTime('+1 day');
        $publishableTrait->setPublishUntil($dateTime);

        $this->assertSame($dateTime, $publishableTrait->getPublishUntil());
    }

    public function isPublishableMethodProvider(): array
    {
        return [
            [OnlineStatus::OFFLINE, null, null, false],
            [OnlineStatus::ONLINE, null, null, true],

            [OnlineStatus::OFFLINE, new \DateTime('-1 hour'), null, false],
            [OnlineStatus::ONLINE, new \DateTime('+1 hour'), null,false],
            [OnlineStatus::ONLINE, new \DateTime('-1 hour'), null, true],

            [OnlineStatus::OFFLINE, null, new \DateTime('+1 hour'), false],
            [OnlineStatus::ONLINE, null, new \DateTime('-1 hour'), false],
            [OnlineStatus::ONLINE, null, new \DateTime('+1 hour'), true],

            [OnlineStatus::OFFLINE, new \DateTime('-1 hour'), new \DateTime('+1 hour'), false],
            [OnlineStatus::ONLINE, new \DateTime('+1 hour'), new \DateTime('+2 hours'), false],
            [OnlineStatus::ONLINE, new \DateTime('-2 hours'), new \DateTime('-1 hour'), false],
            [OnlineStatus::ONLINE, new \DateTime('+1 hour'), new \DateTime('-1 hour'), false],
            [OnlineStatus::ONLINE, new \DateTime('-1 hour'), new \DateTime('+1 hour'), true],
        ];
    }

    /**
     * @dataProvider isPublishableMethodProvider
     */
    public function test_is_publishable_method($status, $publishAt, $publishUntil, $expected): void
    {
        /** @var PublishableTrait $publishableTrait */
        $publishableTrait = $this->getMockForTrait(PublishableTrait::class);

        $publishableTrait->setStatus($status);
        $publishableTrait->setPublishAt($publishAt);
        $publishableTrait->setPublishUntil($publishUntil);

        $this->assertSame($expected, $publishableTrait->isPublishable());
    }
}
