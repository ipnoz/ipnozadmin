<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Tests\unit\Entity\Contracts;

use Codeception\Test\Unit;
use Ipnoz\AdminBundle\Entity\Contracts\OnlineStatus;
use Ipnoz\AdminBundle\Entity\Contracts\OnlineStatusTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class OnlineStatusTraitTest extends Unit
{
    public function test_getter_and_setter(): void
    {
        /** @var OnlineStatusTrait $onlineStatusTrait */
        $onlineStatusTrait = $this->getMockForTrait(OnlineStatusTrait::class);

        $onlineStatusTrait->setStatus(OnlineStatus::ONLINE);

        $this->assertSame(OnlineStatus::ONLINE, $onlineStatusTrait->getStatus());
    }

    public function test_is_online_method(): void
    {
        /** @var OnlineStatusTrait $onlineStatusTrait */
        $onlineStatusTrait = $this->getMockForTrait(OnlineStatusTrait::class);

        $onlineStatusTrait->setStatus(OnlineStatus::OFFLINE);
        $this->assertfalse($onlineStatusTrait->isOnline());

        $onlineStatusTrait->setStatus(OnlineStatus::ONLINE);
        $this->assertTrue($onlineStatusTrait->isOnline());
    }

    public function test_toggle_status_method(): void
    {
        /** @var OnlineStatusTrait $onlineStatusTrait */
        $onlineStatusTrait = $this->getMockForTrait(OnlineStatusTrait::class);

        $onlineStatusTrait->setStatus(OnlineStatus::OFFLINE);

        $this->assertSame(OnlineStatus::ONLINE, $onlineStatusTrait->toggleStatus());
        $this->assertTrue($onlineStatusTrait->isOnline());
    }
}
