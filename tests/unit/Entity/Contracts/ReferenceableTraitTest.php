<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Tests\unit\Entity\Contracts;

use Codeception\Test\Unit;
use Ipnoz\AdminBundle\Entity\Contracts\ReferenceableTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ReferenceableTraitTest extends Unit
{
    public function fieldsProvider(): array
    {
        return [
            'metaTitle' => ['metaTitle'],
            'metaDescription' => ['metaDescription'],
            'metaKeywords' => ['metaKeywords']
        ];
    }

    /**
     * @dataProvider fieldsProvider
     */
    public function test_getters_and_setters($field): void
    {
        /** @var ReferenceableTrait $referenceableTrait */
        $referenceableTrait = $this->getMockForTrait(ReferenceableTrait::class);
        $getter = 'get'.\ucfirst($field);
        $setter = 'set'.\ucfirst($field);

        $referenceableTrait->$setter('test '.$field.' field');

        $this->assertSame('test '.$field.' field', $referenceableTrait->$getter());
    }
}
