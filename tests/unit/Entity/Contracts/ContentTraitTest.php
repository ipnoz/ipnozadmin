<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Tests\unit\Entity\Contracts;

use Codeception\Test\Unit;
use Ipnoz\AdminBundle\Entity\Contracts\ContentTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ContentTraitTest extends Unit
{
    public function test_body_html_getter_and_setter(): void
    {
        /** @var ContentTrait $contentTrait */
        $contentTrait = $this->getMockForTrait(ContentTrait::class);

        $html = '<1>Test&nbsp;&gt;&gt;&nbsp;body</1>';
        $contentTrait->setBodyHtml($html);

        $this->assertSame($html, $contentTrait->getBodyHtml());
        $this->assertSame('Test >> body', $contentTrait->getBody());
    }
}
