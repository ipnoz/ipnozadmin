<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Tests\unit\Entity\Contracts;

use Codeception\Test\Unit;
use Ipnoz\AdminBundle\Entity\Contracts\CreatedAtTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class CreatedTraitTest extends Unit
{
    public function test_created_at_getter_and_setter(): void
    {
        /** @var CreatedAtTrait $createdTrait */
        $createdTrait = $this->getMockForTrait(CreatedAtTrait::class);

        $dateTime  = new \DateTime('-1 day');
        $createdTrait->setCreatedAt($dateTime);

        $this->assertSame($dateTime, $createdTrait->getCreatedAt());
    }

    public function test_updated_at_getter_and_setter(): void
    {
        /** @var CreatedAtTrait $createdTrait */
        $createdTrait = $this->getMockForTrait(CreatedAtTrait::class);

        $dateTime  = new \DateTime('-1 hour');
        $createdTrait->setUpdatedAt($dateTime);

        $this->assertSame($dateTime, $createdTrait->getUpdatedAt());
    }
}
