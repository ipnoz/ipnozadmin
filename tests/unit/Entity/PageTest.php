<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Tests\unit\Entity;

use Codeception\Test\Unit;
use Ipnoz\AdminBundle\Entity\Page;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PageTest extends Unit
{
    public function test_id_getter_and_setter(): void
    {
        $page = new Page();

        $page->setStringId('test string-id field');

        $this->assertSame('test string-id field', $page->getStringId());
    }

    public function test_get_name_method_without_title(): void
    {
        $page = new Page();

        $page->setStringId('test getName method without title');

        $this->assertSame('id : test getName method without title', $page->getAnyName());
    }
}
