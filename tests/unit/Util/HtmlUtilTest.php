<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Tests\unit\Util;

use Codeception\Test\Unit;
use Ipnoz\AdminBundle\Util\HtmlUtil;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class HtmlUtilTest extends Unit
{
    public function test_remove_html_method(): void
    {
        $result = HtmlUtil::removeHtml('<h2>Test html <strong>text</strong> &gt;&gt; string</h2>');

        $this->assertSame('Test html text >> string', $result);
    }

    public function test_remove_html_method_with_null_arg(): void
    {
        $result = HtmlUtil::removeHtml(null);

        $this->assertSame('', $result);
    }

    public function test_remove_html_method_with_nbsp(): void
    {
        $result = HtmlUtil::removeHtml('Test NBSP&nbsp;entity to space');

        $this->assertSame('Test NBSP entity to space', $result);
    }
}
