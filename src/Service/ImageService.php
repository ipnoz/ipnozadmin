<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Ipnoz\AdminBundle\Entity\Image;
use Ipnoz\AdminBundle\Infrastructure\BundleParameters;
use Symfony\Component\Asset\Packages;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ImageService
{
    private $bundleParameters;
    private $em;
    private $slugger;
    private $assets;

    public function __construct(
        BundleParameters $bundleParameters,
        EntityManagerInterface $em,
        SluggerInterface $slugger,
        Packages $package
    ) {
        $this->bundleParameters = $bundleParameters;
        $this->em = $em;
        $this->slugger = $slugger;
        $this->assets = $package;
    }

    public function upload(Image $image): void
    {
        $uploadedFile = $image->getUploadedFile();

        $name = \pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        // Slugify the filename for url
        $filename = $this->slugger->slug($name)->lower();
        $filename = $filename.'-'.\uniqid().'.'.$uploadedFile->getClientOriginalExtension();

        $uploadedFile->move(
            $this->bundleParameters->get('upload_directory').'/images',
            $filename
        );

        $image->setName($name);
        $image->setFileName($filename);
        $image->setExtension($uploadedFile->getClientOriginalExtension());
        $image->setSize($uploadedFile->getSize());
        $image->setUploadedAt(new \DateTime());

        $this->em->persist($image);
        $this->em->flush();
    }

    /**
     * @return Image[]
     */
    public function latest(int $limit, int $offset): array
    {
        return $this->em->getRepository(Image::class)
            ->findBy([], ['uploadedAt' => 'DESC'], $limit, $offset);
    }

    /**
     * @param Image[] $images
     * @return string[]
     */
    public function getOnlyPath(array $images): array
    {
        $return = [];

        foreach ($images as $image) {
            $return[] = [
                'id' => $image->getId(),
                'src' => $this->assets->getUrl('upload/images/'.$image->getFileName())
            ];
        }

        return $return;
    }
}
