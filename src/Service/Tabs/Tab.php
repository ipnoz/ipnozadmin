<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Service\Tabs;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Tab
{
    public $id;
    public $name;
    public $isDefault;
    public $isActive;

    public function __construct(string $id, string $name, bool $isDefault)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isDefault = $isDefault;
        $this->isActive = false;
    }
}
