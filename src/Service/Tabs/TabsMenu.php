<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Service\Tabs;

use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TabsMenu
{
    private $requestStack;
    private $tabs;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->tabs = [];
    }

    /**
     * @return Tab[]
     */
    public function getTabs(): array
    {
        return $this->tabs;
    }

    public function addTab(string $key, string $id, string $name, bool $isDefault = false): void
    {
        $this->tabs[$key] = new Tab($id, $name, $isDefault);
    }

    public function setupActiveTab(): void
    {
        if (empty($this->tabs)) {
            return;
        }

        $requestedTab = null;

        if ($request = $this->requestStack->getCurrentRequest()) {
            if ($request->query->has('tab')) {
                $requestedTab = $request->query->get('tab');
            }
        }

        // Set active the requested tab if it found in the list
        if (is_string($requestedTab)) {

            foreach ($this->tabs as &$tab) {
                if ($requestedTab === $tab->id) {
                    $tab->isActive = true;
                    return;
                }
            }
        }

        // Set active the tab with the isDefault parameter
        foreach ($this->tabs as &$tab) {
            if ($tab->isDefault) {
                $tab->isActive = true;
                return;
            }
        }

        // At least, set active the first tab element
        foreach ($this->tabs as &$tab) {
            $tab->isActive = true;
            break;
        }
    }
}
