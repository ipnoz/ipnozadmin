<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Ipnoz\AdminBundle\Entity\Page;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PageService
{
    private $em;
    private $slugger;

    public function __construct(EntityManagerInterface $em, SluggerInterface $slugger)
    {
        $this->em = $em;
        $this->slugger = $slugger;
    }

    public function new(Page $page): void
    {
        // Slug the title on creation for permalink & stringId
        if (! \is_null($page->getName())) {
            $slugName = $this->slug($page->getName());
        }

        // Slugify the permalink
        if (! \is_null($page->getPermalink())) {
            $slug = $this->slug($page->getPermalink());
            $page->setPermalink($slug);
        } elseif (isset($slugName)) {
            $page->setPermalink($slugName);
        }

        // Slugify the stringId
        if (! \is_null($page->getStringId())) {
            $slug = $this->slug($page->getStringId());
            $page->setStringId($slug);
        } elseif (isset($slugName)) {
            $page->setStringId($slugName);
        }

        if (\is_null($page->getCreatedAt())) {
            $page->setCreatedAt(new \DateTime());
        }

        $this->em->persist($page);
        $this->em->flush();
    }

    public function edit(Page $page): void
    {
        // Slug the permalink on edit
        if (\is_string($page->getPermalink())) {
            $slug = $this->slug($page->getPermalink());
            $page->setPermalink($slug);
        }

        // Slug the stringId on edit
        if (\is_string($page->getStringId())) {
            $slug = $this->slug($page->getStringId());
            $page->setStringId($slug);
        }

        $page->setUpdatedAt(new \DateTime());

        $this->em->flush();
    }

    private function slug(string $sluggable): string
    {
        return $this->slugger->slug($sluggable)->lower()->toString();
    }
}
