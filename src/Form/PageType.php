<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Form;

use Ipnoz\AdminBundle\Entity\Page;
use Ipnoz\AdminBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PageType extends AbstractType
{
    const BLOCK_PREFIX = 'ipnoz_admin_page';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label' => 'Name'
            ])
            ->add('bodyHtml', Type\TextEditorType::class, ['label' => 'Body'])
            ->add('createdAt', Type\CreatedAtType::class)
            ->add('publish', Type\PublishableType::class)
            ->add('permalink', Type\PermalinkType::class)
            ->add('metas', Type\ReferenceableType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Page::class
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return self::BLOCK_PREFIX;
    }
}
