<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Form\TypeGuesser;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmTypeGuesser;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Guess\Guess;
use Symfony\Component\Form\Guess\TypeGuess;
use Symfony\Component\Form\Guess\ValueGuess;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Overriding DoctrineOrmTypeGuesser when guessing string type and the max length
 * is null
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DefaultVarcharLengthGuesser extends DoctrineOrmTypeGuesser
{
    private const DEFAULT_VARCHAR_LENGTH = 255;

    /**
     * {@inheritDoc}
     */
    public function guessType(string $class, string $property): ?TypeGuess
    {
        if (! $ret = $this->getMetadata($class)) {
            return null;
        }

        /** @var ClassMetadata $metadata */
        [$metadata] = $ret;

        if ($metadata->hasAssociation($property)) {
            return null;
        }

        $fieldMapping = $metadata->getFieldMapping($property);

        // Guessing the the max Length constraint for string type
        if (Types::STRING === $fieldMapping['type']) {
            $max = $this->getLength($fieldMapping['length']);
            return new TypeGuess(TextType::class, ['constraints' => new Length(['max' => $max])], Guess::MEDIUM_CONFIDENCE);
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function guessRequired(string $class, string $property): ?ValueGuess
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function guessMaxLength(string $class, string $property): ?ValueGuess
    {
        $ret = $this->getMetadata($class);
        if ($ret && isset($ret[0]->fieldMappings[$property]) && !$ret[0]->hasAssociation($property)) {
            $fieldMapping = $ret[0]->getFieldMapping($property);

            if (Types::STRING === $fieldMapping['type']) {
                $max = $this->getLength($fieldMapping['length']);
                return new ValueGuess($max, Guess::HIGH_CONFIDENCE);
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function guessPattern(string $class, string $property): ?ValueGuess
    {
        return null;
    }

    /**
     * Return the length or the default value on null
     */
    private function getLength(?int $length): int
    {
        return $length ?: self::DEFAULT_VARCHAR_LENGTH;
    }
}
