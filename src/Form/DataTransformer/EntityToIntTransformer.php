<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class EntityToIntTransformer implements DataTransformerInterface
{
    private $em;
    private $entityClass;

    public function __construct(EntityManagerInterface $em, string $entityClass)
    {
        $this->em = $em;
        $this->entityClass = $entityClass;
    }

    /**
     * @inheritDoc
     */
    public function transform($value): ?int
    {
        if (! $value instanceof $this->entityClass) {
            return null;
        }

        return $value->getId();
    }

    /**
     * @inheritDoc
     */
    public function reverseTransform($value)
    {
        if (! $value) {
            return null;
        }

        $entity = $this->em->getRepository($this->entityClass)->findOneBy(['id' => $value]);

        if (! $entity instanceof $this->entityClass) {
            throw new TransformationFailedException(sprintf(
                'A %s with id "%s" does not exist!',
                $this->entityClass,
                $value
            ));
        }

        return $entity;
    }
}
