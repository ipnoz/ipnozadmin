<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Form;

use Ipnoz\AdminBundle\Entity\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class UploadImageType extends AbstractType
{
    const BLOCK_PREFIX = 'ipnoz_admin_upload_image';

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uploadedFile', FileType::class, [
                'required' => true,
                'label' => 'File',
                'constraints' => new File([
                    'mimeTypes' => [
                        'image/*'
                    ],
                    'mimeTypesMessage' => 'Please upload a valid image',
                ]),
                'help' => 'The file must be a valid image'
            ])
            ->add('attrTitle', null, [
                'required' => false,
                'label' => 'Title',
                'help' => 'The title of the image (on mouse hover). This is also useful for SEO'
            ])
            ->add('attrAlt', null, [
                'required' => false,
                'label' => 'Alternative',
                'help' => 'The alternative description of the image when it is not displayed (blinded people, robots etc...). This is also useful for SEO'
            ])
            ->add('submit', SubmitType::class, ['label' => 'Add'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Image::class
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return self::BLOCK_PREFIX;
    }
}
