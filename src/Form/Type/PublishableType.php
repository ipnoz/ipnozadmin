<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Form\Type;

use Ipnoz\AdminBundle\Entity\Contracts\OnlineStatus;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class PublishableType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', RadioInlineType::class, [
                'label' => false,
                'choices' => [
                    'Yes' => OnlineStatus::ONLINE,
                    'No' => OnlineStatus::OFFLINE
                ]
            ])
            ->add('publishAt', DateTimePickerType::class, ['required' => false])
            ->add('publishUntil', DateTimePickerType::class, ['required' => false])
            ->add('submit', SubmitType::class, ['label' => 'Enregistrer'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'inherit_data' => true,
            'label' => 'Publier'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'ipnoz_admin_publish';
    }
}
