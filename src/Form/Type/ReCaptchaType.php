<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 *  (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Form\Type;

use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaConstraint;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ReCaptchaType extends AbstractType
{
    private $language;

    public function __construct(string $language)
    {
        $this->language = $language;
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'mapped' => false,
                'label' => false,
                'inherit_data' => true,
                'error_bubbling' => false,
                'language' => $this->language,
                'constraints' => [
                    new RecaptchaConstraint([
                        'message' => 'The captcha is invalid. Please try again.'
                    ])
                ]
            ])
        ;
    }

    /**
     * @inheritDoc
     */
    public function getParent()
    {
        return EWZRecaptchaType::class;
    }
}
