<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Infrastructure;

use Ipnoz\AdminBundle\DependencyInjection\Configuration;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class BundleParameters
{
    public $parameterBag;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
    }

    public function get(string $key)
    {
        return $this->parameterBag->get(Configuration::ROOT_NAME.'.'.$key);
    }
}
