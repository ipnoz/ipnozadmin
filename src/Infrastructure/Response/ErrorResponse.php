<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Infrastructure\Response;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class ErrorResponse
{
    public $id;
    public $fieldName;
    public $message;

    public function __construct(string $id, string $fieldName, string $message)
    {
        $this->id = $id;
        $this->fieldName = $fieldName;
        $this->message = $message;
    }
}
