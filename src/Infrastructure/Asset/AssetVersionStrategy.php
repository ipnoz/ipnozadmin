<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Infrastructure\Asset;

use Symfony\Component\Asset\VersionStrategy\VersionStrategyInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Asset version based on the md5 of the file contents
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class AssetVersionStrategy implements VersionStrategyInterface
{
    private $parameters;

    public function __construct(ParameterBagInterface $parameters)
    {
        $this->parameters = $parameters;
    }

    public function getVersion($path): string
    {
        return \md5_file($this->getRealpath($path));
    }

    public function applyVersion($path): string
    {
        return $path.'?'.$this->getVersion($path);
    }

    /*
     * Get the real asset path in any situation
     */
    private function getRealpath(string $path): string
    {
        $root = $this->parameters->get('kernel.project_dir');
        return \realpath($root.'/public/'.$path);
    }
}
