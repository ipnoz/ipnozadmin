<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Util;

use Ipnoz\AdminBundle\Infrastructure\Response\ErrorResponse;
use Symfony\Component\Form\FormInterface;

/**
 * Find error messages for all fields from a Symfony form, and storing into an array
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class FormErrorUtil
{
    private $form;
    private $errors;

    public function __construct(FormInterface $form)
    {
        $this->form = $form;
        $this->errors = [];
    }

    public function addError(string $id, string $fieldName, string $message): void
    {
        $this->errors[] = new ErrorResponse($id, $fieldName, $message);
    }

    public function getMessages(): array
    {
        $this->findErrorsMessages($this->form);
        return $this->errors;
    }

    private function findErrorsMessages(FormInterface $form, string $parentId = null): void
    {
        if (is_string($parentId)) {
            $parentId .= '_';
        }

        $id = $parentId.$form->getName();

        if ($form->getErrors()->count() > 0) {
            $this->addError($id, $this->getNiceFormName($form), $form->getErrors()->current()->getMessage());
        }

        foreach ($form->all() as $children) {
            if ($children instanceof FormInterface) {
                $this->findErrorsMessages($children, $id);
            }
        }
    }

    /**
     * Return a nice form name to display to user
     */
    private function getNiceFormName(FormInterface $form): string
    {
        if (! empty($label = $form->getConfig()->getOption('label'))) {
            return $label;
        }

        // The root form name is often not relevant, so return empty value
        return (! $form->isRoot()) ? $form->getName() : '';
    }
}
