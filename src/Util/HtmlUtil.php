<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Util;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class HtmlUtil
{
    /**
     * Remove all HTML from a string
     * Special treatment for &nbps;, see the note
     * https://www.php.net/manual/en/function.html-entity-decode.php#refsect1-function.html-entity-decode-notes
     */
    public static function removeHtml(?string $string): string
    {
        if (is_null($string)) {
            return '';
        }
        return \strip_tags(\html_entity_decode(str_replace('&nbsp;', ' ', $string)));
    }
}
