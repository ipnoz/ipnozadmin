<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Menu;

use Ipnoz\AdminBundle\Controller\PageController;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class BreadcrumbsPageBuilder
{
    private $builder;

    public function __construct(BreadcrumbsBuilder $breadcrumbBuilder)
    {
        $this->builder = $breadcrumbBuilder;
    }

    public function buildIndex(): array
    {
        $this->builder->setCurrentName('Pages');
        return $this->builder->build();
    }

    public function buildNew(): array
    {
        $this->builder->addRoute('Pages', PageController::ROUTE_INDEX);
        $this->builder->setCurrentName('New page');
        return $this->builder->build();
    }

    public function buildEdit(): array
    {
        $this->builder->addRoute('Pages', PageController::ROUTE_INDEX);
        $this->builder->setCurrentName('Edit the page');
        return $this->builder->build();
    }
}
