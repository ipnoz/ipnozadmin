<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Menu;

use Ipnoz\AdminBundle\Event\ConfigureTabsEvent;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class TabsPageBuilder extends TabsBuilderBase
{
    public function build(): array
    {
        $this->menu->addTab('tab1', 'content', 'Content');
        $this->menu->addTab('tab2', 'seo', 'SEO');
        $this->menu->addTab('tab3', 'Parameters', 'Parameters');

        $this->container->get('event_dispatcher')->dispatch(
            new ConfigureTabsEvent($this, $this->menu),
            ConfigureTabsEvent::CONFIGURE
        );

        $this->menu->setupActiveTab();

        return $this->getMenu();
    }
}
