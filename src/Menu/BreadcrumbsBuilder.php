<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class BreadcrumbsBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $factory;

    /*
     * Current route name
     */
    private $currentName;

    /*
     * List of middle routes ["route title" => "route name"]
     */
    private $routes = [];

    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function setCurrentName(string $name): void
    {
        $this->currentName = $name;
    }

    public function addRoute(string $name, string $route): void
    {
        $this->routes[$name] = $route;
    }

    public function build(): array
    {
        $menu = $this->factory->createItem('Breadcrumb');

        $menu->setName($this->currentName);
        $menu->setChildrenAttributes(['class' => 'breadcrumbs pull-left']);

        // Add first route
        $menu->addChild('Dashboard', ['route' => 'ipnoz_admin_default']);

        // Add middle routes
        foreach ($this->routes as $routeName => $route) {
            $menu->addChild($routeName, ['route' => $route]);
        }

        // Add the current and last route
        $menu->addChild($this->currentName);

        return ['breadcrumbsMenu' => $menu];
    }
}
