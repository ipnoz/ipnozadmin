<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Menu;

use Ipnoz\AdminBundle\Service\Tabs\TabsMenu;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class TabsBuilderBase implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    protected $menu;

    public function __construct(TabsMenu $menu)
    {
        $this->menu = $menu;
    }

    public function getMenu(): array
    {
         return ['tabs' => $this->menu->getTabs()];
    }
}
