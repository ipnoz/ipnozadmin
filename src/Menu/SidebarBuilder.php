<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Menu;

use Ipnoz\AdminBundle\Event\ConfigureMenuEvent;
use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class SidebarBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory): ItemInterface
    {
        $menu = $factory->createItem('main');

        $menu->setChildrenAttributes(['id' => 'menu', 'class' => 'metismenu']);
        $menu
            ->addChild('Contenus');
        $menu
            ->addChild('Pages', [
                'route' => 'ipnoz_admin_page_index',
                'extras' => ['icon' => 'ti-layers-alt']
            ]);

        $this->container->get('event_dispatcher')->dispatch(
            new ConfigureMenuEvent($factory, $menu),
            ConfigureMenuEvent::CONFIGURE
        );

        return $menu;
    }
}
