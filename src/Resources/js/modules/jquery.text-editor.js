
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 *
 * Load all text-editor in a page
 * Currently is ckEditor
 */

$(document).ready(function () {

    /*
     * Textarea editors
     */
    let allEditors = document.querySelectorAll('.text-editor');

    for (var i = 0; i < allEditors.length; ++i) {
        ClassicEditor
            .create(allEditors[i], {
                language: 'fr',
                toolbar: {
                    shouldNotGroupWhenFull: true,
                    items: [
                        "undo", "redo", "heading", "|", "bold", "italic", "fontSize", "fontFamily", "fontColor", "fontBackgroundColor", "|", "insertTable", "alignment", "bulletedList", "numberedList", "indent", "outdent", "|", "ckfinder", "mediaEmbed", "link", "|", "blockQuote", "horizontalLine", "removeFormat"
                    ]
                },
                image: {
                    toolbar: ["imageStyle:full", "imageStyle:side", "|", "imageTextAlternative"]
                },
                heading: {
                    options: [
                        {model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph'},
                        {model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1'},
                        {model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2'},
                        {model: 'heading3', view: 'h3', title: 'Heading 3', class: 'ck-heading_heading3'},
                        {model: 'heading4', view: 'h4', title: 'Heading 4', class: 'ck-heading_heading4'},
                        {model: 'heading5', view: 'h5', title: 'Heading 5', class: 'ck-heading_heading5'},
                        {model: 'heading6', view: 'h6', title: 'Heading 6', class: 'ck-heading_heading6'}
                    ]
                },
                ckfinder: {
                    uploadUrl: 'https://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json'
                }
            })
            .catch(error => {
                console.error(error);
            })
        ;
    }
});