
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

let modalId = '#modal-select-media';

function loadLatestImagesInModal(offset, limit)
{
    // We got the last images on the last request
    if (false === offset) {
        return offset;
    }

    let output = false;

    $.ajax({
        type: 'GET',
        url: Routing.generate('ipnoz_admin_image_latest', {
            offset: offset,
            limit: limit,
            format: 'path',
        }),
        dataType: 'json',
        // We have to do synchronous request to return the output updated by the done() promise
        async: false
    }).done(function (response) {

        let originalElement = $(modalId).find('.media.d-none').clone(true);

        $.each(response, function (key, item) {

            // Don't load already loaded images
            if ($(modalId + ' .media[data-image-id="'+ item.id +'"]').length) {
                return;
            }

            let imgElement = originalElement.clone(true);

            imgElement.find('img').attr('src', item.src);
            imgElement.attr('data-image-id', item.id);
            imgElement.removeClass('d-none');

            $(modalId).find('.modal-body .medias-inner').append(imgElement);
        });

        // Update the offset for the next request.
        // If the data sent by server was less than the limit , it's means we
        // reached the last images and we return output with false.
        if (Object.keys(response).length === limit) {
            output = $(modalId+ ' .medias-inner .media').length;
        }
    });

    return output;
}

function selectImageOnForm(media)
{
    $('#image-editor').html(media.find('img').clone());
    // Update image id on the form
    $('input.select-image-media').val(media.data('image-id'));
    // Hide the closest opened modal
    media.closest('.modal.show').modal('hide');
}

$(document).ready(function() {

    let offset = 0;
    let limit = 15;

    // Load image on modal show
    $(modalId).on('show.bs.modal', function() {
        offset = loadLatestImagesInModal(0, limit);
    });

    // Clear the modal on close
    $(modalId).on('hide.bs.modal', function() {
        $(modalId+ ' .medias-inner').text('');
    });

    // Load images when requested by user
    $(modalId+' .display-more-btn').on('click', function() {
        offset = loadLatestImagesInModal(offset, limit);
        if (false === offset) {
            $(this).addClass('disabled');
        }
    });

    // Update the image in the editor and the form input
    $(modalId+' .media').on('click', function() {
        $(modalId+' .media.selected').each(function () {
            $(this).removeClass('selected');
        });
        $(this).addClass('selected');
    });

    $(modalId+' .select-image').on('click', function() {
        selectImageOnForm($(modalId+' .media.selected'));
    });
});
