
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

$(document).ready(function() {

    /*
     * Update the url on tabs click
     */
    $('.nav.nav-tabs .nav-link').on('click', function() {

        let href = new URL(window.location.href);
        href.searchParams.set('tab', $(this).data('id'));

        window.history.pushState('', '', href);
    });
});
