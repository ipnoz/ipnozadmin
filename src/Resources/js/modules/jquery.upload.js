
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

$(document).ready(function() {

    /*
     * Init the bs-custom-file-input
     * This plugin update the file name on the bootstrap 4 file upload element
     */
    bsCustomFileInput.init();

    /*
     * Upload a file with AJAX
     */
    $('.modal-ajax-upload form').on('submit', function(e) {

        e.preventDefault();

        let form = $(this);

        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            dataType: 'json',
            data: new FormData(this),
            processData: false,
            contentType: false,
            beforeSend: function () {

                // Hide error elements from another ajax action
                form.find('.invalid-feedback.d-block').each(function () {
                    $(this).removeClass('d-block');
                });

            }
        }).done(function (data) {

            ajaxFlashMessage('success', data);

            // Reset the form
            form.trigger('reset');
            // Hide the closest opened modal
            form.closest('.modal.show').modal('hide');

        }).fail(function (xhr) {

            /*
             * On fail, display form error message to the related element
             */

            let response = $.parseJSON(xhr.responseText);

            $.each(response, function (key, error) {

                // Add form error message to the related element
                let errorElement = form.find('#'+ error.id +'_ajax_error');
                errorElement.find('.form-error-message').text(error.message);
                // Display the form error element to user
                errorElement.addClass('d-block');
            });
        });
    });
});
