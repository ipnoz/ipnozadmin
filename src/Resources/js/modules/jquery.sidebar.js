
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

$(document).ready(function() {

    /*
     * Enable MetisMenu for the sidebar
     */
    $('#menu').metisMenu();

    /*
     * sidebar collapsing
     */
    if (window.innerWidth <= 991) {
        $('.page-container').addClass('sbar_collapsed');
    }

    $('#btn-toggle-sidebar').on('click', function() {
        $('.page-container').toggleClass('sbar_collapsed');
    });

    /*
     * slimscroll activation
     */
    $('.menu-inner').slimScroll({
        height: 'auto',
        color: '#fff'
    });
    $('.nofity-list').slimScroll({
        height: '435px'
    });
    $('.timeline-area').slimScroll({
        height: '500px'
    });
    $('.recent-activity').slimScroll({
        height: 'calc(100vh - 114px)'
    });
    $('.settings-list').slimScroll({
        height: 'calc(100vh - 158px)'
    });

    // On windows resize, reload the slimScroll
    $(window).resize(function() {
        if(this.resizeTO) clearTimeout(this.resizeTO);
        this.resizeTO = setTimeout(function() {
            $(this).trigger('resizeEnd');
        }, 500);
    });

    $(window).bind('resizeEnd', function() {
        $('.menu-inner').slimScroll({height: 'auto'});
    });

});
