
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

function ajaxFlashMessage(label, message)
{
    let element = $('.ajax-flash-message').clone();

    element.text(message);
    element.addClass('alert-'+ label);
    element.removeClass('d-none');
    $('body').append(element);

    setInterval(function () {
        element.hide(500);
    }, 5000);
}
