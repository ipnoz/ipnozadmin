
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

$(window).on('load', function() {
    setTimeout(function() {
        $('#preloader').fadeOut('slow', function() {
            $(this).remove();
        });
    }, 300);
});
