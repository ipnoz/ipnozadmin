
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

$(document).ready(function () {

    /*
     * DateTime picker editor
     */
    if (typeof daterangepicker === 'function') {

        let format = 'DD/MM/YYYY HH:mm';

        let locales = {
            format: format,
            separator: ' - ',
            applyLabel: 'Choisir',
            cancelLabel: 'Annuler',
            fromLabel: 'À',
            toLabel: 'De',
            customRangeLabel: 'Du/Au',
            weekLabel: 'W',
            daysOfWeek: [
                'Dim.',
                'Lun.',
                'Mar.',
                'Mer.',
                'Jeu.',
                'Ven.',
                'Sam.'
            ],
            monthNames: [
                'Janvier',
                'Février',
                'Mars',
                'Avril',
                'Mai',
                'Juin',
                'Juillet',
                'Août',
                'Septembre',
                'Octobre',
                'Novembre',
                'Décembre'
            ],
        };

        $('.js-editable-modal.datetime-picker').daterangepicker({
            locale: locales,
            singleDatePicker: true,
            showDropdowns: true,
            timePicker: true,
            timePicker24Hour: true,
            autoUpdate: true,
            autoUpdateInput: false
        });

        $('input').on('cancel.daterangepicker', function() {
            $(this).val('');
        });

        /*
         * Workaround
         * bootstrap-daterangepicker with "singleDatePicker" option and
         * "autoUpdate" to false don't update the picked date. Do it with this.
         */
        $('input').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format(format));
        });
    }

    /*
     * Edit input modal
     */
    $('.js-editable-modal.text-modal').on('click', function () {

        let input = $(this);
        let modal = $('#modal-edit-input');
        let modalInput = $('#modal-edit-input .modal-body textarea');
        let help = input.parent().find('.form-text.text-muted').text();

        // Update the help element of the modal
        modal.find('.help').text(help);

        // Update the modal input value before open it
        modalInput.val(input.val());
        modal.modal();

        // Update the target input value on submit
        modal.find('#modal-confirm-action').on('click', function () {
            input.val(modalInput.val());
        });
    });

    /*
     * Delete modal
     */
    $('#modal-delete-item').on('show.bs.modal', function (e) {

        let modal = $(this);
        let link = $(e.relatedTarget);

        /*
         * Replace the %name% variable in the modal description element with
         * the data-name element of the related link.
         * It's important to keep the original text if we want to reuse the modal
         */
        let originalText = modal.find('.modal-body .original-description').html();
        let replacedText = originalText.replace('%name%', link.data('name'));
        modal.find('.modal-body .description').html(replacedText);

        modal.find('#modal-confirm-action').on('click', function () {
            document.location.href = link.attr('href');
        });
    });
});
