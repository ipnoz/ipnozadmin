
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

'use strict';

$(document).ready(function() {

    /*
     * Fullscreen Page
     */

    if ($('#full-view').length) {

        const requestFullscreen = function(element) {
            if (element.requestFullscreen) {
                element.requestFullscreen();
            } else if (element.webkitRequestFullscreen) {
                element.webkitRequestFullscreen();
            } else if (element.mozRequestFullScreen) {
                element.mozRequestFullScreen();
            } else if (element.msRequestFullscreen) {
                element.msRequestFullscreen();
            } else {
                console.error('Fullscreen API is not supported.');
            }
        };

        const exitFullscreen = function() {
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else {
                console.error('Fullscreen API is not supported.');
            }
        };

        const fsDocButton = document.getElementById('full-view');
        const fsExitDocButton = document.getElementById('full-view-exit');

        fsDocButton.addEventListener('click', function(e) {
            e.preventDefault();
            requestFullscreen(document.documentElement);
        });

        fsExitDocButton.addEventListener('click', function(e) {
            e.preventDefault();
            exitFullscreen();
        });

        document.addEventListener('fullscreenchange', function() {
            if (document.fullScreen || document.mozFullScreen || document.webkitIsFullScreen) {
                $('body').addClass('expanded');
            } else {
                $('body').removeClass('expanded');
            }
        });
    }
});
