<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Event;

use Ipnoz\AdminBundle\Service\Tabs\TabsMenu;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ConfigureTabsEvent extends Event
{
    const CONFIGURE = 'ipnoz_admin.tabs_configure';

    private $builderClass;
    private $tabs;

    public function __construct(object $builder, TabsMenu $tabs)
    {
        $this->builderClass = \get_class($builder);
        $this->tabs = $tabs;
    }

    public function getBuilderClassName(): string
    {
        return $this->builderClass;
    }

    public function getTabsMenu(): TabsMenu
    {
        return $this->tabs;
    }
}
