<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ipnoz\AdminBundle\Entity\Contracts;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 *
 * @ORM\MappedSuperclass()
 */
class Page implements Contracts\Referenceable, Contracts\Publishable
{
    use Contracts\IdTrait;
    use Contracts\ContentTrait;
    use Contracts\PermalinkTrait;
    use Contracts\ReferenceableTrait;
    use Contracts\PublishableTrait;

    /**
     * @var string
     * @ORM\Column(unique=true, nullable=true)
     */
    private $stringId;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $name;


    public function getStringId(): ?string
    {
        return $this->stringId;
    }

    public function setStringId(?string $stringId): void
    {
        $this->stringId = $stringId;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getAnyName(): string
    {
        return $this->name ?: ($this->stringId ? 'id : '.$this->stringId : 'id : '.$this->id);
    }
}
