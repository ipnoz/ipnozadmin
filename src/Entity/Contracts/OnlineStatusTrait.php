<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity\Contracts;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait OnlineStatusTrait
{
    /**
     * @var string
     * @ORM\Column(length=16)
     */
    private $status = OnlineStatus::OFFLINE;


    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    public function isOnline(): bool
    {
        return OnlineStatus::ONLINE === $this->status;
    }

    public function toggleStatus(): string
    {
        $this->status = ($this->isOnline()) ? OnlineStatus::OFFLINE : OnlineStatus::ONLINE;
        return $this->status;
    }
}
