<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity\Contracts;

use Doctrine\ORM\Mapping as ORM;
use Ipnoz\AdminBundle\Util\HtmlUtil;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait ContentTrait
{
    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $body;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $bodyHtml;

    public function getBody(): ?string
    {
        return $this->body;
    }

    public function getBodyHtml(): ?string
    {
        return $this->bodyHtml;
    }

    public function setBodyHtml(?string $bodyHtml): void
    {
        $this->bodyHtml = $bodyHtml;
        $this->body = HtmlUtil::removeHtml($bodyHtml);
    }
}
