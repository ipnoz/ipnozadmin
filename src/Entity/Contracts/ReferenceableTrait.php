<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity\Contracts;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait ReferenceableTrait
{
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $metaTitle;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaDescription;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $metaKeywords;


    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }
}
