<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity\Contracts;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface Publishable extends OnlineStatus
{
    public function getCreatedAt(): ?\DateTime;
    public function setCreatedAt(\DateTime $createdAt): void;

    public function getUpdatedAt(): ?\DateTime;
    public function setUpdatedAt(\DateTime $updatedAt): void;

    public function getPublishAt(): ?\DateTime;
    public function setPublishAt(\DateTime $publishAt): void;

    public function getPublishUntil(): ?\DateTime;
    public function setPublishUntil(\DateTime $publishUntil): void;

    public function isPublishable(): bool;
}
