<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity\Contracts;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface Media
{
    public function getFileName(): ?string;
    public function setFileName(string $fileName): void;

    public function getName(): ?string;
    public function setName(string $name): void;

    public function getExtension(): ?string;
    public function setExtension(string $extension): void;

    public function getSize(): ?int;
    public function setSize(int $size): void;

    public function getUploadedFile(): ?UploadedFile;
    public function setUploadedFile(UploadedFile $uploadedFile): void;

    public function getUploadedAt(): ?\DateTime;
    public function setUploadedAt(\DateTime $uploadedAt): void;
}
