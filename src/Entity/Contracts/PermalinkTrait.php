<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity\Contracts;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait PermalinkTrait
{
    /**
     * @var string
     * @ORM\Column(unique=true, nullable=true)
     */
    private $permalink;

    public function getPermalink(): ?string
    {
        return $this->permalink;
    }

    public function setPermalink(?string $permalink): void
    {
        $this->permalink = $permalink;
    }
}
