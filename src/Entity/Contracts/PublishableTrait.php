<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity\Contracts;

use Doctrine\ORM\Mapping as ORM;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
trait PublishableTrait
{
    use OnlineStatusTrait;
    use CreatedAtTrait;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $publishUntil;

    public function getPublishAt(): ?\DateTime
    {
        return $this->publishAt;
    }

    public function setPublishAt(?\DateTime $publishAt): void
    {
        $this->publishAt = $publishAt;
    }

    public function getPublishUntil(): ?\DateTime
    {
        return $this->publishUntil;
    }

    public function setPublishUntil(?\DateTime $publishUntil): void
    {
        $this->publishUntil = $publishUntil;
    }

    public function isPublishable(): bool
    {
        $now = new \DateTime();

        return ($this->isOnline()) &&
            (\is_null($this->publishAt) || $this->publishAt->getTimestamp() < $now->getTimestamp()) &&
            (\is_null($this->publishUntil) || $this->publishUntil->getTimestamp() > $now->getTimestamp());
    }
}
