<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity\Contracts;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface OnlineStatus
{
    const ONLINE = 'online';
    const OFFLINE = 'offline';

    public function getStatus(): ?string;
    public function setStatus(string $status): void;

    public function isOnline(): bool;
    public function toggleStatus(): string;
}
