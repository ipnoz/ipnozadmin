<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity\Contracts;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
interface Referenceable
{
    public function getMetaTitle(): ?string;
    public function setMetaTitle(string $metaTitle): void;

    public function getMetaDescription(): ?string;
    public function setMetaDescription(string $metaDescription): void;

    public function getMetaKeywords(): ?string;
    public function setMetaKeywords(string $metaKeywords): void;
}
