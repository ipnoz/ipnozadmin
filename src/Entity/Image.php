<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ipnoz\AdminBundle\Entity\Contracts;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 *
 * @ORM\MappedSuperclass()
 */
class Image implements Contracts\Media
{
    use Contracts\IdTrait;
    use Contracts\MediaTrait;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    private $attrTitle;

    /**
     * @var string
     * @ORM\Column(nullable=true)
     */
    private $attrAlt;

    public function getAttrTitle(): ?string
    {
        return $this->attrTitle;
    }

    public function setAttrTitle(?string $attrTitle): void
    {
        $this->attrTitle = $attrTitle;
    }

    public function getAttrAlt(): ?string
    {
        return $this->attrAlt;
    }

    public function setAttrAlt(?string $attrAlt): void
    {
        $this->attrAlt = $attrAlt;
    }
}
