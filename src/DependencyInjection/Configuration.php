<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Configuration implements ConfigurationInterface
{
    const ROOT_NAME = 'ipnoz_admin';

    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder(self::ROOT_NAME);
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->scalarNode('upload_directory')->defaultValue('%kernel.project_dir%/public/upload')->cannotBeEmpty()->info('Upload directory for all medias')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
