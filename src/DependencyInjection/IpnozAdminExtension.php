<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class IpnozAdminExtension extends Extension implements PrependExtensionInterface
{
    private $configLocator;

    public function __construct()
    {
        $this->configLocator = new FileLocator(__DIR__.'/../Resources/config');
    }

    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        // Load bundle services
        $xmlFileLoader = new XmlFileLoader($container, $this->configLocator);
        $xmlFileLoader->load('services.xml');

        // Setup bundle configuration in the container
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $container->setParameter(Configuration::ROOT_NAME.'.upload_directory', $config['upload_directory']);
    }

    /**
     * @inheritDoc
     */
    public function prepend(ContainerBuilder $container)
    {
        // Load others bundles configuration
        $yamlFileLoader = new YamlFileLoader($container, $this->configLocator);
        $yamlFileLoader->import('packages/*.yaml');
    }
}
