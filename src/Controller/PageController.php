<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Ipnoz\AdminBundle\Entity\Page;
use Ipnoz\AdminBundle\Form\PageType;
use Ipnoz\AdminBundle\Menu\BreadcrumbsPageBuilder;
use Ipnoz\AdminBundle\Menu\TabsPageBuilder;
use Ipnoz\AdminBundle\Service\PageService;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 *
 * @Route("/page", name="page_")
 */
class PageController extends BaseController
{
    const ROUTE_BASE = 'ipnoz_admin_page_';
    const ROUTE_INDEX = self::ROUTE_BASE.'index';
    const ROUTE_EDIT = self::ROUTE_BASE.'edit';

    private $service;
    private $breadcrumbsBuilder;
    private $tabsBuilder;

    public function __construct(
        PageService $service,
        BreadcrumbsPageBuilder $breadcrumbBuilder,
        TabsPageBuilder $tabsBuilder
    ) {
        $this->service = $service;
        $this->breadcrumbsBuilder = $breadcrumbBuilder;
        $this->tabsBuilder = $tabsBuilder;
    }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function indexAction(EntityManagerInterface $em): Response
    {
        $pages = $em->getRepository(Page::class)->findAll();

        return $this->render('@IpnozAdmin/Page/index.html.twig', [
                'pages' => $pages
            ]
            +$this->breadcrumbsBuilder->buildIndex()
        );
    }

    /**
     * @Route("/new", name="new", methods={"GET", "POST"})
     */
    public function newAction(Request $request): Response
    {
        $page = new Page();

        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->service->new($page);
                $this->addFlash('success', 'La page a été créée');
                return $this->redirectToRoute(self::ROUTE_EDIT, ['id' => $page->getId()]);
            } else {
                $this->addFormErrorFlash($form);
            }
        }

        return $this->render('@IpnozAdmin/Page/form.html.twig', [
                'form' => $form->createView()
            ]
            +$this->breadcrumbsBuilder->buildNew()
            +$this->tabsBuilder->build()
        );
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Page $page): Response
    {
        $form = $this->createForm(PageType::class, $page);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->service->edit($page);
                $this->addFlash('success', 'La page a été éditée');
                return $this->redirectToReferer($request);
            } else {
                $this->addFormErrorFlash($form);
            }
        }

        return $this->render('@IpnozAdmin/Page/form.html.twig', [
                'form' => $form->createView()
            ]
            +$this->breadcrumbsBuilder->buildEdit()
            +$this->tabsBuilder->build()
        );
    }

    /**
     * Toggle status of the page
     *
     * @Route("/state/{id}", name="state", methods={"GET"})
     */
    public function stateAction(Page $page, EntityManagerInterface $em): RedirectResponse
    {
        $page->toggleStatus();
        $em->flush();

        return $this->redirectToRoute(self::ROUTE_INDEX);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"GET"})
     */
    public function deleteAction(Page $page): RedirectResponse
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($page);
        $em->flush();

        $this->addFlash('success', 'La page "'.$page->getName().'" a été supprimée');

        return $this->redirectToRoute(self::ROUTE_INDEX);
    }
}
