<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class DefaultController extends BaseController
{
    /**
     * @Route("/", name="default")
     */
    public function indexAction()
    {
        return $this->render('@IpnozAdmin/index.html.twig');
    }
}
