<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Controller;

use Ipnoz\AdminBundle\Entity\Image;
use Ipnoz\AdminBundle\Form\UploadImageType;
use Ipnoz\AdminBundle\Service\ImageService;
use Ipnoz\AdminBundle\Util\FormErrorUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 *
 * @Route("/image", name="image_")
 */
class ImageController extends BaseController
{
//    /**
//     * @Route("/", name="index", methods={"GET"})
//     */
//    public function indexAction(EntityManagerInterface $em): Response
//    {
//        $pages = $em->getRepository(Page::class)->findAll();
//
//        $this->breadcrumbsBuilder->setCurrentName('Pages');
//
//        return $this->render('@IpnozAdmin/Page/index.html.twig', [
//            'pages' => $pages
//        ]+
//            $this->setupBreadcrumbs($this->breadcrumbsBuilder->build())
//        );
//    }

    /**
     * @Route("/upload", name="upload", methods={"POST"})
     */
    public function uploadAction(Request $request, ImageService $service): JsonResponse
    {
        $image = new Image();

        $form = $this->createForm(UploadImageType::class, $image);
        $form->handleRequest($request);

        $formErrorUtil = new FormErrorUtil($form);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $service->upload($image);
                return new JsonResponse('Success');
            } catch (\Exception $e) {
                // Adding the exception at the form errors to return to user
                $formErrorUtil->addError($form->getName(), '', $e->getMessage());
            }
        }

        return new JsonResponse($formErrorUtil->getMessages(), 500);
    }

    /**
     * @Route("/latest", name="latest", methods={"GET"}, options={"expose"=true})
     */
    public function latestAction(Request $request, ImageService $service): JsonResponse
    {
        $format = $request->query->get('format');
        $offset = $request->query->getInt('offset');
        $limit = $request->query->getInt('limit', 10);

        $images = $service->latest($limit, $offset);
        if ('path' === $format) {
            $images = $service->getOnlyPath($images);
        }

        return new JsonResponse($images);
    }


//
//    /**
//     * @Route("/edit/{id}", name="edit", methods={"GET", "POST"})
//     */
//    public function editAction(Request $request, Page $page): Response
//    {
//        $form = $this->createForm(PageType::class, $page, ['action_edit' => true]);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $this->service->editPage($page);
//            return $this->redirectToRoute($request->get('_route'), ['id' => $page->getId()]);
//        }
//
//        $this->breadcrumbsBuilder->addRoute('Pages', 'ipnoz_admin_page_index');
//        $this->breadcrumbsBuilder->setCurrentName('Édition de la page');
//
//        return $this->render('@IpnozAdmin/Page/form.html.twig', [
//            'form' => $form->createView()
//            ]+
//            $this->setupBreadcrumbs($this->breadcrumbsBuilder->build())
//        );
//    }
//
//    /**
//     * Toggle status of the page
//     *
//     * @Route("/state/{id}", name="state", methods={"GET"})
//     */
//    public function stateAction(Page $page, EntityManagerInterface $em): RedirectResponse
//    {
//        $page->toggleStatus();
//        $em->flush();
//
//        return $this->redirectToRoute('ipnoz_admin_page_index');
//    }
//
//    /**
//     * @Route("/delete/{id}", name="delete", methods={"GET"})
//     */
//    public function deleteAction(Page $page): RedirectResponse
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $em->remove($page);
//        $em->flush();
//
//        $this->addFlash('success', 'La page "'.$page->getTitle().'" a été supprimée');
//
//        return $this->redirectToRoute('ipnoz_admin_page_index');
//    }
}
