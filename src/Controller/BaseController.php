<?php

/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Ipnoz\AdminBundle\Controller;

use Ipnoz\AdminBundle\Form\UploadImageType;
use Ipnoz\AdminBundle\Util\FormErrorUtil;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
abstract class BaseController extends AbstractController
{
    /**
     * Helper to redirect to the referer
     */
    protected function redirectToReferer(Request $request): RedirectResponse
    {
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Helper to setup the form of upload image
     */
    protected function setupUploadImageForm(): array
    {
        $form = $this->createForm(UploadImageType::class, null, ['action' => $this->generateUrl('ipnoz_admin_image_upload')]);
        return ['uploadImageForm' => $form->createView()];
    }

    /**
     * Change classical flash message type
     */
    protected function addFlash(string $type, $message): void
    {
        switch ($type) {
            case 'success':
                $type = 'info';
                break;
            case 'error':
                $type = 'danger';
                break;
        }
        parent::addFlash($type, $message);
    }

    /**
     * Helper for form error flash messages
     */
    protected function addFormErrorFlash(FormInterface $form): void
    {
        $formErrorUtil = new FormErrorUtil($form);
        $this->addFlash('formValidationError', $formErrorUtil->getMessages());
    }
}
