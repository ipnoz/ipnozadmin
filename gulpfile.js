
/*
 * This file is part of the Ipnoz Admin bundle.
 *
 * (c) David Dadon <david.dadon@ipnoz.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

'use strict';

const browserSync = require('browser-sync').create();
const del = require('del');
const gulp = require('gulp');
const autoprefixer = require('gulp-autoprefixer');
const preserveTime = require('gulp-preservetime');
const rename = require("gulp-rename");
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const uglifyjs = require('uglify-es');
const composer = require('gulp-uglify/composer');
const jsMinifyier = composer(uglifyjs, console);

const NODE_MODULES = './node_modules';
const TMP = './var/cache/dev/gulp';
const SRC = './src/Resources';
const DEST = './src/Resources/public';
const DEST_BUILD = DEST +'/build';

const START_PATH = __dirname.replace('/var/www/html', '')+'/../public';

/*
 * Functions
 */
function copyVendors() {
    del([DEST + '/vendor/ckeditor5/**/*']);

    return gulp.src([NODE_MODULES + '/ckeditor5-build-classic-all-plugin/build/**/*'])
        .pipe(preserveTime())
        .pipe(gulp.dest(DEST + '/vendor/ckeditor5/build-classic-all-plugin'));
}

function compileScss()
{
    return gulp
        .src(SRC+'/scss/**/*.scss')
        .pipe(sass.sync({outputStyle:'compressed'}).on('error', sass.logError))
        .pipe(autoprefixer("last 2 versions", "> 1%", "Explorer 9", "Android 2"))
        .pipe(cleanCSS({inline: 'none'}))
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(DEST_BUILD))
        .pipe(browserSync.stream());
}

function concatJs()
{
    del([TMP+'/*']);

    return gulp.src(SRC + '/js/**/*.js')
        .pipe(concat('admin.js'))
        .pipe(gulp.dest(TMP));
}

function minifyJs()
{
    return gulp.src(TMP + '/*.js')
        .pipe(jsMinifyier())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(DEST_BUILD));
}

function watchFiles()
{
    gulp.watch(SRC+'/scss/**/*.scss', gulp.series('css'));
    gulp.watch(SRC+'/js/**/*.js', gulp.series('js'));
}

function browserSyncDaemon()
{
    browserSync.init({
        proxy: '172.18.0.1',
        open: false,
        notify: false,
        logFileChanges: false,
        reloadOnRestart: true,
        ghostMode: false,
        startPath: START_PATH,
    });
}

/*
 * Task
 */
gulp.task('css', gulp.series(compileScss));
gulp.task('js', gulp.series(concatJs, minifyJs));
gulp.task('vendors', gulp.series(copyVendors));

gulp.task('build', gulp.series('css', 'js', 'vendors'));
gulp.task('default', gulp.parallel(browserSyncDaemon, watchFiles));
